# Connecting to a Spin App

In order to be able to access your application from outside of Spin (for
example from a web browser, over the Internet) you must tell Spin how to route
incoming requests. This can be done by configuring an Ingress. It is similar to
the configuration used by a web server, which translates URLs to file system
paths.

## HTTP-based services: Ingresses

If you want users to access an endpoint that speaks the `HTTP` protocol (ie. a
web server, or an API), go to the `Resources` menu, select `Workloads` and then
click the `Load Balancer` tab. Click `Add Ingress`, give the ingress a name,
select the namespace serviced by the ingress and check `Specify a hostname to
use`.

For `Request Host`, enter the following (substitute your information as needed):

```
<name>.<namespace>.<environment>.svc.spin.nersc.org
```

Where:

- name can be anything (except contain a period)

- namespace is the selected namespace

- environment is the current one ("production" or "development")

- Note that the domain ends in ".org" not ".gov"

Next, leave `path` blank, select the HTTP-based workload you want to access and
enter a port.

Click "Save" and wait for the state to go from "Initializing" to "Ready". Wait
another 1-5 mins for DNS to propagate and then try accessing your app at

```
http://<name>.<namespace>.<environment>.svc.spin.nersc.org:<port>
```

### HTTPS access and Custom DNS names

DOE security requirements dictate that web services must be encrypted using
a web certificate. Generally, obtaining a certificate will be done in 
conjunction with obtaining a custom DNS name for your app. Processes for
obtaining certificates and registering DNS names vary from institution
to institution, so check with your local IT support or project collaborators
for guidance.

Your custom DNS name should simply be a `CNAME` record to the long, generated
name above ending in `svc.spin.nersc.org`; for example,

```
<custom DNS name> CNAME <name>.<namespace>.<environment>.svc.spin.nersc.org
```

Once you have a web certificate and custom DNS name, edit the ingress
and add a rule to specify the DNS name to use and select the same port
and target as the ingress. You should now be able to access your application via:

```
http://<custom DNS name>
```

To install the web certificate, click "Resources",
select "Secrets" and click the "Certificates" tab. Click "Add Certificate" and
enter the private key and the certificate. Next go to "Workloads", select "Load
Balancing", find and edit your ingress and select the certificate in the "SSL/TLS"
panel. You should now be able to access your application via:

```
https://<custom DNS name>
```

## Non-HTTP services: Load Balancers

Services such as MySQL, PostgreSQL and other non-HTTP applications can also be
opened to other networks at NERSC. Note that access to these applications are
restricted to NERSC networks only, although we are working on a method to
expose these services to other facilities.

For this example, let's assume that you have set up a `mysql` workload called
`db` and want to make connect to it from the Cori or Spin login nodes.

Edit your `db` workload, and add the following to `Port Mapping`, and hit `Save`:

  * Port Name: mysql
  * Publish the container port: 3306
  * Protocol: TCP
  * As a `Layer-4 Load Balancer`
  * On listening port: 3306

Notice that your workload now has loadbalancer Service called `db-loadbalancer`
and its state is `active`. The service will have an automatically generated
name in the format `<workload>-loadbalancer`. Spin will automatically generate
the DNS hostname for your service, in the following format:

```
<workload>-loadbalancer.<namespace>.<environment>.svc.spin.nersc.org
```

Click on `⋮` > `Edit` of the `db-loadbalancer`, click on `Show advanced options`
and open `Labels and Annotations`. Notice that the hostname for your service is
automatically set with the following annotation. This is the hostname to use
for accessing your application.

```
external-dns.alpha.kubernetes.io/hostname=<name>.<namespace>.<environment>.svc.spin.nersc.org
```

Optionally, click on the `Service Discovery` tab and notice there are two
Services: `db` that exposes a port within your namespace, and `db-loadbalancer`
which is the same service that you saw under the `Load Balancing` tab.

!!! warning "Ports allowed for non-HTTP services"
    Only the following ports can be accessed when using non-HTTP services: 873, 3128, 3306, 4873, 5432, 5672, 5984, 27017, 15672
    These ports cover standard ports for databases and popular network services.

### External Traffic Policy

A feature called `External Traffic Policy` controls how traffic from sources
outside of Spin is routed to your application. There are two choices:

* The `Local` policy is is faster then `Cluster`, as your loadbalancer endpoint
  will reside on the same node as your service. It will also preserve the
  client source IP for applications that need it.
* The `Cluster` policy provides better network redundancy, but at the expense
  of one network hop which reduces performance. It also obscures the client
  soucre IP.

For most services, we recommend that you set this to `Local`.  To do this, on
your loadbalancer service, click on `⋮` > `Edit`.  Scroll down to `External
Traffic Policy ` and select `Local`.

To connect to your external hostname, use the above hostname and the
application port. For example for a hypothetical mysql workload:

```
mysqlsh -h db-loadbalancer.<namespace>.<environment>.svc.spin.nersc.org -u root
```
